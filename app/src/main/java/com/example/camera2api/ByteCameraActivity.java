package com.example.camera2api;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.camera2api.camera.ImageCaptureCallback;
import com.example.camera2api.views.ByteCamera;

import java.nio.ByteBuffer;

public class ByteCameraActivity extends AppCompatActivity {

    ByteCamera byteCamera;
    ImageView imageViewCaptured;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_byte_camera);

        byteCamera = findViewById(R.id.byte_camera);
        imageViewCaptured = findViewById(R.id.image_captured);

        byteCamera.setImageCaptureCallback(new ImageCaptureCallback() {
            @Override
            public void onImageCaptureCompleted(Image image) {
                ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                byte[] bytes = new byte[buffer.capacity()];
                buffer.get(bytes);
                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                imageViewCaptured.setImageBitmap(bitmap);
                imageViewCaptured.setVisibility(View.VISIBLE);
            }

            @Override
            public void onImageCropCompleted(Bitmap bitmap) {

            }
        });
    }
}
