package com.example.camera2api;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void openCameraActivity(View view) {
        Intent intent = new Intent(this, DrawableCameraActivity.class);
        startActivity(intent);
    }

    public void openByteCamera(View view) {
        Intent intent = new Intent(this, ByteCameraActivity.class);
        startActivity(intent);
    }
}