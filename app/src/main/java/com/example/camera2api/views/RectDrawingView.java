package com.example.camera2api.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

public class RectDrawingView extends View {

    private static final int REFRESH_DISTANCE = 8;

    private int relX_start = 0;
    private int relY_start = 0;
    private int relX_end = 0;
    private int relY_end = 0;

    private int absX_start = 0;
    private int absY_start = 0;
    private int absX_end = 0;
    private int absY_end = 0;

    private boolean shouldRectReDraw = false;
    private Paint rectPaint;
    private TextPaint textPaint;
    private Listener listener;

    public RectDrawingView(Context context) {
        super(context);
        init();
    }

    public RectDrawingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RectDrawingView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public RectDrawingView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        rectPaint = new Paint();
        rectPaint.setColor(getContext().getResources().getColor(android.R.color.holo_green_light));
        rectPaint.setStyle(Paint.Style.STROKE);
        rectPaint.setStrokeWidth(5);

        textPaint = new TextPaint();
        textPaint.setColor(getContext().getResources().getColor(android.R.color.holo_green_light));
        textPaint.setTextSize(20);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                shouldRectReDraw = false;
                relX_start = (int) event.getX();
                relY_start = (int) event.getY();
                relX_end = relX_start;
                relY_end = relY_start;

                absX_start = (int) event.getRawX();
                absY_start = (int) event.getRawX();

                break;

            case MotionEvent.ACTION_MOVE:
                int relX = (int) event.getX();
                int relY = (int) event.getY();

                if(!new Rect(getLeft(), getTop(), getRight(), getBottom()).contains(relX, relY)) break;

                if (!shouldRectReDraw || Math.abs(relX - relX_end) > REFRESH_DISTANCE || Math.abs(relY - relY_end) > REFRESH_DISTANCE) {
                    if(Math.abs(relX - relX_start) > REFRESH_DISTANCE || Math.abs(relY - relY_start) > REFRESH_DISTANCE) {
                        shouldRectReDraw = true;
                        relX_end = relX;
                        relY_end = relY;
                        absX_end = (int) event.getRawX();
                        absY_end = (int) event.getRawY();

                        if(listener != null) listener.onRectChanged(getAbsoluteRect(), getRelativeRect());
                        invalidate();
                    }
                }

                break;

            case MotionEvent.ACTION_UP:
                relX = (int) event.getX();
                relY = (int) event.getY();

                if(Math.abs(relX - relX_start) > REFRESH_DISTANCE || Math.abs(relY - relY_start) > REFRESH_DISTANCE) {
                    if (listener != null) {
                        listener.onRectFinished(getAbsoluteRect(), getRelativeRect());
                    }
                    invalidate();
                }
        }

        return true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if(!shouldRectReDraw) return;

        canvas.drawRect(getRelativeRect(), rectPaint);
        canvas.drawText(getRelativeRect().toShortString(), Math.max(relX_end, relX_start), Math.max(relY_end, relY_start), textPaint);
    }

    private Rect getRelativeRect() {
        return new Rect(Math.min(relX_start, relX_end), Math.min(relY_start, relY_end), Math.max(relX_end, relX_start), Math.max(relY_start, relY_end));
    }

    private Rect getAbsoluteRect() {
        return new Rect(Math.min(absX_start, absX_end), Math.min(absY_start, absY_end), Math.max(absX_end, absX_start), Math.max(absY_start, absY_end));
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public interface Listener {
        void onRectChanged(Rect absoluteRect, Rect relativeRect);
        void onRectFinished(Rect absoluteRect, Rect relativeRect);
    }
}
