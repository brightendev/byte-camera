package com.example.camera2api.views;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.camera2api.R;
import com.example.camera2api.camera.CameraView;
import com.example.camera2api.camera.ImageCaptureCallback;

public class RectDrawableCamera extends FrameLayout {

    private CameraView cameraView;

    public RectDrawableCamera(@NonNull Context context) {
        super(context);
        init();
    }

    public RectDrawableCamera(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RectDrawableCamera(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public RectDrawableCamera(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init(){
        inflate(getContext(), R.layout.layout_rect_drawable_camera,this);
        cameraView = findViewById(R.id.camera);
    }

    public void captureImage(ImageCaptureCallback captureCallback) {
        cameraView.captureImage(captureCallback);
    }

    public void toggleFlashMode() {
        cameraView.toggleFlashMode();
    }

    public void setFlashModeListener(CameraView.FlashModeListener listener) {
        cameraView.setFlashModeListener(listener);
    }

    public void setZoomLevel(float level) {
        cameraView.setZoomLevel(level);
    }
}
