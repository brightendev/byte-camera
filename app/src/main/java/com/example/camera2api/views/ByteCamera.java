package com.example.camera2api.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.SeekBar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;

import com.example.camera2api.R;
import com.example.camera2api.camera.CameraView;
import com.example.camera2api.camera.FlashMode;
import com.example.camera2api.camera.ImageCaptureCallback;

public class ByteCamera extends FrameLayout {

    private CameraView camera;
    private Button buttonCapture;
    private SeekBar seekZoom;
    private ImageButton buttonFlash;
    private RectDrawingView rectDrawingView;
    private ImageCaptureCallback imageCaptureCallback;

    public ByteCamera(@NonNull Context context) {
        super(context);
        init();
    }

    public ByteCamera(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ByteCamera(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public ByteCamera(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.layout_byte_camera,this);

        camera = findViewById(R.id.camera);
        buttonCapture = findViewById(R.id.button_capture);
        seekZoom = findViewById(R.id.seek_zoom);
        buttonFlash = findViewById(R.id.button_flash);
        rectDrawingView = findViewById(R.id.rect_drawing);

        camera.setFlashModeListener(new CameraView.FlashModeListener() {
            @Override
            public void onFlashModeChanged(FlashMode flashMode) {
                switch(flashMode) {
                    case OFF:
                        buttonFlash.setImageResource(R.drawable.ic_flash_off);
                        break;
                    case ON:
                        Drawable icon = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_flash_on, null);
                        icon.setTint(Color.YELLOW);
                        buttonFlash.setImageDrawable(icon);
                }
            }
        });
        buttonCapture.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                camera.captureImage(new ImageCaptureCallback() {
                    @Override
                    public void onImageCaptureCompleted(Image image) {
                        if(imageCaptureCallback == null) return;

                        imageCaptureCallback.onImageCaptureCompleted(image);
                    }

                    @Override
                    public void onImageCropCompleted(Bitmap bitmap) {
                        if(imageCaptureCallback == null) return;

                        imageCaptureCallback.onImageCropCompleted(bitmap);
                    }
                });
            }
        });
        seekZoom.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                camera.setZoomLevel(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
        buttonFlash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                camera.toggleFlashMode();
            }
        });
        rectDrawingView.setListener(new RectDrawingView.Listener() {
            @Override
            public void onRectChanged(Rect absoluteRect, Rect relativeRect) {
                Log.d("RectDrawingViewListener", "onRectChanged: "+ relativeRect);
            }

            @Override
            public void onRectFinished(Rect absoluteRect, Rect relativeRect) {
                Log.d("RectDrawingViewListener", "onRectFinished: "+ relativeRect);
            }
        });
    }

    public void setImageCaptureCallback(ImageCaptureCallback imageCaptureCallback) {
        this.imageCaptureCallback = imageCaptureCallback;
    }
}
