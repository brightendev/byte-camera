package com.example.camera2api.camera;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.ImageFormat;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.ImageReader;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class CameraProcessor {

    private final Context context;
    private String currentCameraId;
    private ImageReader imageReader;
    private Size previewSize = new Size(0, 0);
    private CameraDevice cameraDevice;
    private CaptureRequest.Builder captureRequestBuilder;
    private CameraCaptureSession captureSession;
    private int sensorOrientation;

    private Rect cameraSensorActiveRect;
    private float maximumZoomLevel;
    private float zoomLevel = 1.0f;
    private FlashMode flashMode;

    CameraProcessor(Context context) {
        this.context = context;
    }

    Size getPreviewSize() {
        return previewSize;
    }

    void openCamera(SurfaceTexture outputsurface, int width, int height) {
        final int REQUEST_CAMERA_PERMISSION = 200;
        if(ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String[]{ Manifest.permission.CAMERA }, REQUEST_CAMERA_PERMISSION);
            return;
        }

        setUpOutputs(width, height);

        CameraManager manager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);
        try {
            CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
                @Override
                public void onOpened(@NonNull CameraDevice cameraDevice) {
                    CameraProcessor.this.cameraDevice = cameraDevice;
                    createCameraPreviewSession(outputsurface);
                }

                @Override
                public void onDisconnected(@NonNull CameraDevice cameraDevice) {
                    cameraDevice.close();
                    cameraDevice = null;
                }

                @Override
                public void onError(@NonNull CameraDevice cameraDevice, int error) {
                    cameraDevice.close();
                    cameraDevice = null;
                }
            };
            manager.openCamera(currentCameraId, stateCallback, null);
        }
        catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    void closeCamera() {
        if (captureSession != null) {
            captureSession.close();
            captureSession = null;
        }
        if (cameraDevice != null) {
            cameraDevice.close();
            cameraDevice = null;
        }
        if (imageReader != null) {
            imageReader.close();
            imageReader = null;
        }
    }

    private void createCameraPreviewSession(SurfaceTexture surfaceTexture) {
        try {
            surfaceTexture.setDefaultBufferSize(previewSize.getWidth(), previewSize.getHeight());
            Surface outputSurface = new Surface(surfaceTexture);

            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            captureRequestBuilder.addTarget(outputSurface);

            cameraDevice.createCaptureSession(Arrays.asList(outputSurface, imageReader.getSurface()), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession session) {
                    if(cameraDevice == null) return;

                    captureSession = session;

                    try {
                        captureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                        captureSession.setRepeatingRequest(captureRequestBuilder.build(), null, null);
                    }
                    catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession session) {
                    Toast.makeText(context, "Failed to configure camera.", Toast.LENGTH_SHORT).show();
                }
            }, null);
        }
        catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void setUpOutputs(int width, int height) {
        CameraManager manager = (CameraManager) context.getSystemService(Context.CAMERA_SERVICE);

        try {
            for(String id : manager.getCameraIdList()) {
                CameraCharacteristics characteristics = manager.getCameraCharacteristics(id);

                // Skip front camera
                Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
                if(facing != null && facing == CameraCharacteristics.LENS_FACING_FRONT) {
                    continue;
                }

                StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                if(map == null) continue;

                cameraSensorActiveRect = characteristics.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE);
                maximumZoomLevel = characteristics.get(CameraCharacteristics.SCALER_AVAILABLE_MAX_DIGITAL_ZOOM);
                Size largest = Collections.max(Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)), new AreaComparator());

                // ==== Set up image output ====
                imageReader = ImageReader.newInstance(largest.getWidth(), largest.getHeight(), ImageFormat.JPEG, 2);


                // === Set up preview output ===
                final int MAX_PREVIEW_WIDTH = 1920;
                final int MAX_PREVIEW_HEIGHT = 1080;

                // Find out if we need to swap dimension to get the preview size relative to sensor coordinate.
                WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                int displayRotation = wm.getDefaultDisplay().getRotation();
                //noinspection ConstantConditions
                sensorOrientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
                boolean swappedDimensions = false;
                switch (displayRotation) {
                    case Surface.ROTATION_0:
                    case Surface.ROTATION_180:
                        if (sensorOrientation == 90 || sensorOrientation == 270) {
                            swappedDimensions = true;
                        }
                        break;
                    case Surface.ROTATION_90:
                    case Surface.ROTATION_270:
                        if (sensorOrientation == 0 || sensorOrientation == 180) {
                            swappedDimensions = true;
                        }
                        break;
                    default:
                        Log.e("TAG", "Display rotation is invalid: " + displayRotation);
                }

                Point displaySize = new Point();
                wm.getDefaultDisplay().getSize(displaySize);
                int rotatedPreviewWidth = width;
                int rotatedPreviewHeight = height;
                int maxPreviewWidth = displaySize.x;
                int maxPreviewHeight = displaySize.y;

                if (swappedDimensions) {
                    rotatedPreviewWidth = height;
                    rotatedPreviewHeight = width;
                    maxPreviewWidth = displaySize.y;
                    maxPreviewHeight = displaySize.x;
                }

                if (maxPreviewWidth > MAX_PREVIEW_WIDTH) {
                    maxPreviewWidth = MAX_PREVIEW_WIDTH;
                }

                if (maxPreviewHeight > MAX_PREVIEW_HEIGHT) {
                    maxPreviewHeight = MAX_PREVIEW_HEIGHT;
                }

                // Danger, W.R.! Attempting to use too large a preview size could  exceed the camera
                // bus' bandwidth limitation, resulting in gorgeous previews but the storage of
                // garbage capture data.
                previewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class), rotatedPreviewWidth, rotatedPreviewHeight, maxPreviewWidth, maxPreviewHeight, largest);

                // ===
                currentCameraId = id;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Given {@code choices} of {@code Size}s supported by a camera, choose the smallest one that
     * is at least as large as the respective texture view size, and that is at most as large as the
     * respective max size, and whose aspect ratio matches with the specified value. If such size
     * doesn't exist, choose the largest one that is at most as large as the respective max size,
     * and whose aspect ratio matches with the specified value.
     *
     * @param choices           The list of sizes that the camera supports for the intended output
     *                          class
     * @param textureViewWidth  The width of the texture view relative to sensor coordinate
     * @param textureViewHeight The height of the texture view relative to sensor coordinate
     * @param maxWidth          The maximum width that can be chosen
     * @param maxHeight         The maximum height that can be chosen
     * @param aspectRatio       The aspect ratio
     * @return The optimal {@code Size}, or an arbitrary one if none were big enough
     */
    private static Size chooseOptimalSize(Size[] choices, int textureViewWidth, int textureViewHeight, int maxWidth, int maxHeight, Size aspectRatio) {

        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<>();
        // Collect the supported resolutions that are smaller than the preview Surface
        List<Size> notBigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getWidth() <= maxWidth && option.getHeight() <= maxHeight &&
                    option.getHeight() == option.getWidth() * h / w) {
                if (option.getWidth() >= textureViewWidth &&
                        option.getHeight() >= textureViewHeight) {
                    bigEnough.add(option);
                }
                else {
                    notBigEnough.add(option);
                }
            }
        }

        // Pick the smallest of those big enough. If there is no one big enough, pick the
        // largest of those not big enough.
        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new AreaComparator());
        }
        else if (notBigEnough.size() > 0) {
            return Collections.max(notBigEnough, new AreaComparator());
        }
        else {
            Log.e("CameraProcessor", "Couldn't find any suitable preview size");
            return choices[0];
        }
    }

    private static class AreaComparator implements Comparator<Size> {
        @Override
        public int compare(Size s1, Size s2) {
            return Integer.compare(s1.getWidth()*s1.getHeight(), s2.getWidth()*s2.getHeight());
        }
    }

    private void captureStillImage(ImageReader.OnImageAvailableListener onImageAvailableListener) {
        try {
            CaptureRequest.Builder captureBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);

            imageReader.setOnImageAvailableListener(onImageAvailableListener, null);

            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            int rotation = wm.getDefaultDisplay().getRotation();
            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, getOrientation(rotation));
            captureBuilder.addTarget(imageReader.getSurface());
            captureBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
            captureBuilder.set(CaptureRequest.SCALER_CROP_REGION, getZoomRect(zoomLevel));
            if(flashMode == FlashMode.ON) {
                captureBuilder.set(CaptureRequest.FLASH_MODE, CameraMetadata.FLASH_MODE_SINGLE);
            }

            captureSession.capture(captureBuilder.build(), null, null);

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }


    void captureImage(ImageCaptureCallback captureCallback) {
        captureStillImage(new ImageReader.OnImageAvailableListener() {
            @Override
            public void onImageAvailable(ImageReader reader) {
                captureCallback.onImageCaptureCompleted(reader.acquireLatestImage());
            }
        });
    }

    void captureImage(ImageCaptureCallback captureCallback, Rect croppingRect) {
        captureStillImage(new ImageReader.OnImageAvailableListener() {
            @Override
            public void onImageAvailable(ImageReader reader) {
                captureCallback.onImageCaptureCompleted(reader.acquireLatestImage());
            }
        });
    }

    private Rect getZoomRect(float level) {
        if(cameraSensorActiveRect == null) return new Rect();

        float zoom = (level / 100.0f) * maximumZoomLevel;
        float ratio = (float) 1 / zoom;
        int croppedWidth = cameraSensorActiveRect.width() - Math.round((float)cameraSensorActiveRect.width() * ratio);
        int croppedHeight = cameraSensorActiveRect.height() - Math.round((float)cameraSensorActiveRect.height() * ratio);

        return new Rect(croppedWidth/2, croppedHeight/2,
                cameraSensorActiveRect.width() - croppedWidth/2, cameraSensorActiveRect.height() - croppedHeight/2);
    }

    /**
     * Set zoom level as percentage.
     * @param level Zoom level as percentage
     */
    void setZoomLevel(float level) {
        if(cameraSensorActiveRect == null || captureRequestBuilder == null) return;

        this.zoomLevel = level;
        try {
            captureRequestBuilder.set(CaptureRequest.SCALER_CROP_REGION, getZoomRect(level));
            captureSession.setRepeatingRequest(captureRequestBuilder.build(), null, null);
        }
        catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    void setFlashMode(FlashMode flashMode) {
        if(captureRequestBuilder == null || captureSession == null) return;

        this.flashMode = flashMode;
        try {
            captureRequestBuilder.set(CaptureRequest.FLASH_MODE, flashMode.getValue());
            captureSession.setRepeatingRequest(captureRequestBuilder.build(),null, null);
        }
        catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    // === Retrieves the JPEG orientation from the specified screen rotation. ===
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }
    private int getOrientation(int rotation) {
        return (ORIENTATIONS.get(rotation) + sensorOrientation + 270) % 360;
    }
}
