package com.example.camera2api.camera;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.media.Image;
import android.media.ImageReader;
import android.util.AttributeSet;
import android.util.Size;
import android.view.Surface;
import android.view.TextureView;
import android.view.WindowManager;

public class CameraView extends TextureView implements TextureView.SurfaceTextureListener {

    private final Context context;
    private final CameraProcessor cameraProcessor;

    private int ratioWidth = 0;
    private int ratioHeight = 0;
    private FlashMode flashMode = FlashMode.OFF;
    private FlashModeListener flashModeListener;

    public CameraView(Context context) {
        super(context);
        this.context = context;
        cameraProcessor = new CameraProcessor(context);
    }

    public CameraView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        cameraProcessor = new CameraProcessor(context);
    }

    public CameraView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        cameraProcessor = new CameraProcessor(context);
    }

    public CameraView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
        cameraProcessor = new CameraProcessor(context);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        setSurfaceTextureListener(this);
    }

    @Override
    public void onSurfaceTextureAvailable(SurfaceTexture surface, int width, int height) {
        cameraProcessor.openCamera(surface, width, height);
        configureTransform(width, height, cameraProcessor.getPreviewSize());
        setAspectRatio();
    }

    @Override
    public void onSurfaceTextureSizeChanged(SurfaceTexture surface, int width, int height) {
        configureTransform(width, height, cameraProcessor.getPreviewSize());
    }

    @Override
    public boolean onSurfaceTextureDestroyed(SurfaceTexture surface) {
        cameraProcessor.closeCamera();
        return false;
    }

    @Override
    public void onSurfaceTextureUpdated(SurfaceTexture surface) {

    }

    private void configureTransform(int viewWidth, int viewHeight, Size previewSize) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        int rotation = wm.getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, previewSize.getHeight(), previewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();

        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max((float) viewHeight/previewSize.getHeight(), (float) viewWidth/previewSize.getWidth());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        }
        else if (Surface.ROTATION_180 == rotation) {
            matrix.postRotate(180, centerX, centerY);
        }
        this.setTransform(matrix);
    }

    /**
     * Sets the aspect ratio for this view. The size of the view will be measured based on the ratio
     * calculated from the parameters. Note that the actual sizes of parameters don't matter, that
     * is, calling setAspectRatio(2, 3) and setAspectRatio(4, 6) make the same result.
     *
     */
    private void setAspectRatio() {
        int orientation = context.getResources().getConfiguration().orientation;
        Size previewSize = cameraProcessor.getPreviewSize();

        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            ratioWidth = previewSize.getWidth();
            ratioHeight = previewSize.getHeight();
        }
        else {
            ratioWidth = previewSize.getHeight();
            ratioHeight = previewSize.getWidth();
        }
        requestLayout();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        int width = MeasureSpec.getSize(widthMeasureSpec);
        int height = MeasureSpec.getSize(heightMeasureSpec);
        if (0 == ratioWidth || 0 == ratioHeight) {
            setMeasuredDimension(width, height);
        }
        else {
            if (width < height * ratioWidth / ratioHeight) {
                setMeasuredDimension(width, width * ratioHeight / ratioWidth);
            }
            else {
                setMeasuredDimension(height * ratioWidth / ratioHeight, height);
            }
        }
    }

    public void captureImage(ImageCaptureCallback captureCallback) {
        cameraProcessor.captureImage(captureCallback);
    }

    public FlashMode getFlashMode() {
        return flashMode;
    }

    public void toggleFlashMode() {
        flashMode = flashMode.toggle();
        cameraProcessor.setFlashMode(flashMode);

        if(flashModeListener != null) flashModeListener.onFlashModeChanged(flashMode);
    }

    public void setFlashModeListener(FlashModeListener listener) {
        this.flashModeListener = listener;
    }

    /**
     * Set zoom level as percentage.
     * @param level Zoom level as percentage
     */
    public void setZoomLevel(float level) {
        cameraProcessor.setZoomLevel(level);
    }

    public interface FlashModeListener {
        void onFlashModeChanged(FlashMode flashMode);
    }
}
