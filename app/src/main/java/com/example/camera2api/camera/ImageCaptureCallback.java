package com.example.camera2api.camera;

import android.graphics.Bitmap;
import android.media.Image;

public interface ImageCaptureCallback {
    void onImageCaptureCompleted(Image image);
    void onImageCropCompleted(Bitmap bitmap);
}
