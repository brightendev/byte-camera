package com.example.camera2api.camera;

import android.hardware.camera2.CameraMetadata;

public enum FlashMode {
    OFF(CameraMetadata.FLASH_MODE_OFF), ON(CameraMetadata.FLASH_MODE_TORCH);

    private int value;

    FlashMode(int value) {
        this.value = value;
    }

    FlashMode toggle() {
      if(this.equals(OFF)) return ON;
      return OFF;
    }

    int getValue() {
        return value;
    }
}
