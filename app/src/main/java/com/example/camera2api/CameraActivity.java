package com.example.camera2api;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import com.example.camera2api.camera.CameraView;
import com.example.camera2api.camera.FlashMode;
import com.example.camera2api.camera.ImageCaptureCallback;
import com.example.camera2api.views.RectDrawingView;

import java.nio.ByteBuffer;

public class CameraActivity extends AppCompatActivity {

    private CameraView cameraView;
    private ImageView imageViewCaptured;
    private SeekBar seekZoom;
    private ImageButton buttonFlash;
    private RectDrawingView rectDrawingView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);

        cameraView = findViewById(R.id.cameraview);
        imageViewCaptured = findViewById(R.id.image_captured);
        buttonFlash = findViewById(R.id.button_flash);

        seekZoom = findViewById(R.id.seek_zoom);
        seekZoom.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                cameraView.setZoomLevel(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        buttonFlash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraView.toggleFlashMode();
            }
        });
        cameraView.setFlashModeListener(new CameraView.FlashModeListener() {
            @Override
            public void onFlashModeChanged(FlashMode flashMode) {
                switch(flashMode) {
                    case OFF:
                        buttonFlash.setImageResource(R.drawable.ic_flash_off);
                        break;
                    case ON:
                        Drawable icon = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_flash_on, null);
                        icon.setTint(Color.YELLOW);
                        buttonFlash.setImageDrawable(icon);
                }
            }
        });

        rectDrawingView = findViewById(R.id.rect);
        rectDrawingView.setListener(new RectDrawingView.Listener() {
            @Override
            public void onRectChanged(Rect absoluteRect, Rect relativeRect) {
                Log.d("RectDrawingViewListener", "onRectChanged: "+ absoluteRect);
            }

            @Override
            public void onRectFinished(Rect absoluteRect, Rect relativeRect) {
                Log.d("RectDrawingViewListener", "onRectFinished: "+ absoluteRect);
            }
        });
    }

    public void captureImage(View view) {
        cameraView.captureImage(new ImageCaptureCallback() {
            @Override
            public void onImageCaptureCompleted(Image image) {
                ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                byte[] bytes = new byte[buffer.capacity()];
                buffer.get(bytes);
                Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                imageViewCaptured.setImageBitmap(bitmap);
                imageViewCaptured.setVisibility(View.VISIBLE);
            }

            @Override
            public void onImageCropCompleted(Bitmap bitmap) {

            }
        });
    }
}
